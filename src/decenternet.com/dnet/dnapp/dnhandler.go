package dnapp

import (
	"io"
	"net"
	"bufio"
	"log"
	"github.com/davecgh/go-spew/spew"
	"time"
	"encoding/json"
)

func (app *App) HandleConn(conn net.Conn) {

	defer conn.Close()

	io.WriteString(conn, "Node Name:")

	scanner := bufio.NewScanner(conn)

	go func() {
		for scanner.Scan() {
			bpm := scanner.Text()

			newBlock, err := app.GenerateBlock(hashBlock[len(hashBlock)-1], bpm)
			if err != nil {
				log.Println(err)
				continue
			}
			if app.IsHBlockValid(newBlock, hashBlock[len(hashBlock)-1]) {
				newBlockNodes := append(hashBlock, newBlock)
				app.ReplaceChain(newBlockNodes)
			}

			bcServer <- hashBlock
			io.WriteString(conn, "\nNode List Name:")
		}
	}()

	// Test purpose only, send json data every 5secs
	go func() {
		for {
			time.Sleep(5 * time.Second)
			mutex.Lock()
			output, err := json.Marshal(hashBlock)
			if err != nil {
				log.Fatal(err)
			}
			mutex.Unlock()
			log.Println("SEND->",string(output))
			io.WriteString(conn, string(output))
		}
	}()

	for _ = range bcServer {
		spew.Dump(hashBlock)
	}

}
