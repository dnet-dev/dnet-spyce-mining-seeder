package dnapp

import (
	"net/http"
	"github.com/bmizerany/pat"
)

func (app *App) Routes() http.Handler {
	mux := pat.New()

	mux.Get("/",http.HandlerFunc(app.HandleGetHBlock))
	mux.Post("/",http.HandlerFunc(app.HandleWriteHBlock))

	return mux
}