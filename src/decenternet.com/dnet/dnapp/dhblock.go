package dnapp

import (
	"strconv"
	"crypto/sha256"
	"encoding/hex"
	"time"
	"net/http"
	"encoding/json"
	"sync"
	"github.com/davecgh/go-spew/spew"
	"io"
)

var mutex = &sync.Mutex{}

var bcServer chan []HBlock
var hashBlock []HBlock

// SHA256
func (app *App) GenerateHash(hblock HBlock) string {
	nNode := strconv.Itoa(hblock.Index) + hblock.Timestamp + hblock.Node + hblock.PrevHash
	h := sha256.New()
	h.Write([]byte(nNode))
	hashed := h.Sum(nil)
	return hex.EncodeToString(hashed)
}

func (app *App) GenerateBlock(prevBlock HBlock, Node string) (HBlock,error) {
	var newBlock HBlock

	t := time.Now()

	newBlock.Index = prevBlock.Index + 1
	newBlock.Timestamp = t.String()
	newBlock.Node = Node
	newBlock.PrevHash = prevBlock.Hash
	newBlock.Hash = app.GenerateHash(newBlock)

	return newBlock, nil
}

func (app *App) IsHBlockValid(newBlock, prevBlock HBlock) bool {
	if prevBlock.Index+1 != newBlock.Index {
		return false
	}

	if prevBlock.Hash != newBlock.PrevHash {
		return false
	}

	if app.GenerateHash(newBlock) != newBlock.Hash {
		return false
	}

	return true
}

func respondWithJSON(w http.ResponseWriter, r *http.Request, code int, payload interface{}) {
	response, err := json.MarshalIndent(payload, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("HTTP 500: Internal Server Error"))
		return
	}
	w.WriteHeader(code)
	w.Write(response)
}

func (app *App) HandleWriteHBlock(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var m HNode

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&m); err != nil {
		respondWithJSON(w, r, http.StatusBadRequest, r.Body)
		return
	}
	defer r.Body.Close()

	mutex.Lock()
	newBlock, err := app.GenerateBlock(hashBlock[len(hashBlock)-1], m.Node)
	if err != nil {
		panic(err)
	}

	mutex.Unlock()

	if app.IsHBlockValid(newBlock, hashBlock[len(hashBlock)-1]) {
		hashBlock = append(hashBlock, newBlock)
		spew.Dump(hashBlock)
	}

	respondWithJSON(w, r, http.StatusCreated, newBlock)

}

func (app *App) HandleGetHBlock(w http.ResponseWriter, r *http.Request) {
	bytes, err := json.MarshalIndent(hashBlock, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.WriteString(w, string(bytes))
}

func (app *App) LoadHashBlock() {
	t := time.Now()

	genBlock := HBlock{}
	genBlock = HBlock{0,t.String(),"",app.GenerateHash(genBlock),""}
	spew.Dump(genBlock)

	mutex.Lock()
	hashBlock = append(hashBlock, genBlock)
	mutex.Unlock()
}


func (app *App) ReplaceChain(newBlocks []HBlock) {
	mutex.Lock()
	if len(newBlocks) > len(hashBlock) {
		hashBlock = newBlocks
	}
	mutex.Unlock()
}