package dnapp


type App struct {}

type DNAppSettings struct {
	DSN string  // for database
	PORT string // port to connect
}

type HBlock struct {
	Index int
	Timestamp string
	Node string
	Hash string
	PrevHash string
}

type HNode struct {
	Node string
}

