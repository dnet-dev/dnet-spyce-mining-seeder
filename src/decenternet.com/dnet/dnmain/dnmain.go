package main

import (
	"log"
	"net/http"
	"decenternet.com/dnet/dnconfig"
	"net"
)


func startHttpService() {

	app := &dnconfig.DNETConfig{}
	config, err := app.InitConfig()

	config.LoadHashBlock()
	log.Println("** HTTP Decenternet",app.Port())
	err = http.ListenAndServe(app.Port(), config.Routes())
	log.Fatal(err)
}

func startTCPService() {

	app := &dnconfig.DNETConfig{}
	config, err := app.InitConfig()

	config.LoadHashBlock()
	log.Println("** TCP Decenternet",app.Port())
	server, err := net.Listen("tcp", app.Port())
	if err != nil {
		log.Fatal(err)
	}

	defer server.Close()

	for {
		conn, err := server.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go config.HandleConn(conn)
	}
}

func main() {

	startTCPService()
}
