package dnconfig

import (
	"decenternet.com/dnet/dnapp"
	"flag"
	"decenternet.com/dnet/dnconstant"
)

type DNETConfig struct {}

var appSettings *dnapp.DNAppSettings

func (cfg *DNETConfig) InitConfig() (*dnapp.App,error) {
	appSettings = new(dnapp.DNAppSettings)
	flag.StringVar(&appSettings.DSN, "dsn",dnconstant.Dsn, "database dsn")
	flag.StringVar(&appSettings.PORT, "port",dnconstant.Port, "http port")
	flag.Parse()


	app := &dnapp.App{}

	return app, nil
}

func (cfg *DNETConfig) Port() string {
	return appSettings.PORT
}